for %%* in (.) do set CurrDirName=%%~nx*
docker kill %CurrDirName%
docker rm %CurrDirName%
docker run --name %CurrDirName% -p 3000:3000 -d mermade/%CurrDirName%:latest
